<?php

    try {  
      $DBH = new PDO("sqlite:db/trackTracker");  
    }  
    catch(PDOException $e) {  
        echo $e->getMessage();  
    }

date_default_timezone_set('UTC');
$date   = date('Y/m/d H:i:s');
$artist = $_POST["string"]["artist"];
$title  = $_POST["string"]["title"];
$vote   = $_POST["string"]["vote"];

echo $artist;

if ($artist && $title && $vote) {
$sql= "SELECT track_id FROM TRACKS WHERE artist = :artist and title = :title";
$stmt = $DBH->prepare($sql);
$stmt->bindParam(':artist', $artist, PDO::PARAM_STR);
$stmt->bindParam(':title', $title, PDO::PARAM_STR);
$stmt->execute();

echo $stmt;


}

// Let's clean up
$DBH = null;
?>
